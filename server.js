(function() {
    "use strict";
    var express = require('express');
    var fs = require('fs');
    var app = express();

    var ipaddress = "127.0.0.1";
    var port = 3000;
    var webapp = "/webcontent";

    app.configure(function() {
        //app.set('view engine', 'jade');
        app.use(express.bodyParser());        
        app.use(express.static(__dirname + webapp)); 
        app.use(express.methodOverride());
        app.use(allowCrossDomain);
        app.use(app.router);
    });

    app.configure('development', function() {
        app.use(express.errorHandler({dumpExceptions: true, showStack: true}));
    });

    app.configure('production', function() {
        app.use(express.errorHandler());
    });

    
    //=====================================================================
    app.get('/ping', function(request, response) {
        return response.send("QNReck");
    });
    //=====================================================================
        

    app.listen(port, ipaddress, function() {
        console.log('Express server listen on port ' + port);
    });

    function allowCrossDomain(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, PUT, POST, DELETE');
        res.header('Access-Control-Allow-Headers', 'Content-Type');
        next();
    }
})();
